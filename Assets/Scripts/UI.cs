using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    public GameObject pausa;
 

    public void Pausa()
    {
        
        pausa.SetActive(true);
    }

    public void Return()
    {
        
        pausa.SetActive(false);
    }

    public void GoMenu()
    {
        
        SceneManager.LoadScene("Menu");
    }
    
    public void Exit()
    {
        Application.Quit();
    }

    public void More()
    {
      
        SceneManager.LoadScene("Credits");
    }

    public void Retry()
    {
     
        SceneManager.LoadScene("Game");
    }
    public void History()
    {
        SceneManager.LoadScene("History2");
    }

    public void History2()
    {
        SceneManager.LoadScene("History3");
    }
}
