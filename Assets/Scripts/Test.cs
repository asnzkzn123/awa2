using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Test : MonoBehaviour
{
    [Header("Tarjetas")]
    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;
    public Button uno;
    public Button dos;
    public TextMeshProUGUI value1;
    public TextMeshProUGUI value2;

    [Header("Fade")]
    public GameObject fade;
    public GameObject fade2;

    public GameObject ftexto1;
    public GameObject ftexto2;

    public GameObject timagen1;
    public GameObject timagen2;

    public GameObject boton1;
    public GameObject boton2;
    public GameObject boton3;

    [Header("Fade Gota")]
    public CanvasGroup gota1;
    public CanvasGroup gota2;
    public GameObject texto1G;
    public GameObject texto2G;

    [Header("Boton1")]
    
    public Sprite ImageA1;
    public Sprite ImageA2;
    public Sprite ImageA3;
    public Sprite ImageA4;
    public Sprite ImageA5;
    public Sprite ImageA6;
    public Sprite ImageA7;
   
    [Header("Boton2")]

    public Sprite ImageB1;
    public Sprite ImageB2;
    public Sprite ImageB3;
    public Sprite ImageB4;
    public Sprite ImageB5;
    public Sprite ImageB6;
    public Sprite ImageB7;

    [Header("Barra")]

    public TextMeshProUGUI barra;

    public int etapa = 0;
    string[] texto;
    string[] texto2;
    int[] valor1;
    int[] valor2;
    float score = 100;
    Sprite[] imagenes;
    Sprite[] imagenes2;
    public Slider slider;

    float timer = 0;


    Animator anim;

    GameObject pelado;



    // Start is called before the first frame update
    void Start()
    {
        //fade.GetComponent<Image>().enabled = false;
        //fade2.GetComponent<Image>().enabled = false;
        
        pelado = GameObject.FindGameObjectWithTag("Pelado");
        anim = pelado.gameObject.GetComponent<Animator>();


        slider.value = 1;

        texto = new string[] { "AGUA PARA LA MASCOTA","LAVAR LA ROPA" , "CEPILLARSE LOS DIENTES" , "COCINAR" , "TOMAR AGUA" , "DUCHARSE" , "LAVAR MOTOTAXI"};
        texto2 = new string[] { "BA�AR A LA MASCOTA","LAVAR LOS SERVICIOS" , "BALDE DE AGUA EN EL INODORO", "LAVAR LOS ALIMENTOS" , "DARLE AGUA A TU HIJO" , "BA�AR A TU HIJO" , "TRAPEAR EL PISO"};
        valor1 = new int[] { 10,10,5,20,10,30,20};
        valor2 = new int[] {15,10,10,15,10,25,15};
        imagenes = new Sprite[] { ImageA1,ImageA2,ImageA3,ImageA4,ImageA5,ImageA6,ImageA7};
        imagenes2 = new Sprite[] { ImageB1, ImageB2,ImageB3,ImageB4,ImageB5,ImageB6,ImageB7};

        //por defecto
        text1.text = texto[etapa];
        text2.text = texto2[etapa];

        value1.text = valor1[etapa].ToString() + "%";
        value2.text = valor2[etapa].ToString()+ "%";

        barra.text = score.ToString() + "%";

        timagen1.GetComponent<Image>().sprite = imagenes[etapa];
        timagen2.GetComponent<Image>().sprite = imagenes2[etapa];
        Debug.Log("La etapa " + etapa);
        uno.onClick.AddListener(TaskOnClick);

        dos.onClick.AddListener(TaskOnClick2);
        

    }
    void TaskOnClick()
    {

        score -= valor1[etapa];
        slider.value = score/100;
       

        if (score < 0)
        {
            Debug.Log("PERDISTE");
            StartCoroutine(change_fade(false));
        }
        if (score >= 0 && etapa == 6)
        {
            Debug.Log("GANASTE");
            StartCoroutine(change_fade(true));

        }

        if (etapa < 6)
        {
            etapa++;
        }
        else
        {
            return;
        }
        Debug.Log("La etapa " + etapa);

        StartCoroutine(change_text());

        StartCoroutine(show_gota(true));


        StartCoroutine(change_image());
        StartCoroutine(change_porc());


        anim.SetTrigger("C_Izq");

    }

    void TaskOnClick2()
     {
        score -= valor2[etapa];
        slider.value = score/100;

        if (score < 0)
        {
            Debug.Log("PERDISTE");
            StartCoroutine(change_fade(false));
        }

        if (score >= 0 && etapa == 6)
        {
            Debug.Log("GANASTE");
            StartCoroutine(change_fade(true));
        }

        if (etapa < 6)
        {
            etapa++;
        }
        else
        {
            return;
        }

        StartCoroutine(change_text());

        StartCoroutine(show_gota(false));


        StartCoroutine(change_image());

        StartCoroutine(change_porc());

        Debug.Log("La etapa " + etapa);

        anim.SetTrigger("C_Der");


    }

    IEnumerator change_fade(bool ganaste)
    {
        if (ganaste)
        {
            fade2.SetActive(true);
            fade2.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
            fade2.GetComponent<Image>().CrossFadeAlpha(1, 3, true);
            yield return new WaitForSeconds(2);

            ftexto2.SetActive(true);
            boton1.SetActive(true);
        }
        else
        {
            fade.SetActive(true);
            fade.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
            fade.GetComponent<Image>().CrossFadeAlpha(1, 3, true);
            yield return new WaitForSeconds(2);

            ftexto1.SetActive(true);
            boton2.SetActive(true);
            boton3.SetActive(true);

        }

    }

    IEnumerator show_gota(bool select)
    {
        
        if (select)
        {
            texto1G.SetActive(false);
            gota1.alpha = 1;
            yield return new WaitForSeconds(0.5f);

            value1.text = valor1[etapa].ToString() + "%";

            gota1.alpha = 0;
            texto1G.SetActive(true);

        }

        else
        {
            texto2G.SetActive(false);
            gota2.alpha = 1;
            yield return new WaitForSeconds(0.5f);

            value2.text = valor2[etapa].ToString() + "%";

            gota2.alpha = 0;
            texto2G.SetActive(true);

        }

    }

    IEnumerator change_image()
    {
        yield return new WaitForSeconds(0.5f);

        timagen1.GetComponent<Image>().sprite = imagenes[etapa];
        timagen2.GetComponent<Image>().sprite = imagenes2[etapa];
    }

    IEnumerator change_porc()
    {
        yield return new WaitForSeconds(0f);

        barra.text = score.ToString() + "%";
    }

    IEnumerator change_text()
    {
        yield return new WaitForSeconds(0.5f);


        text1.text = texto[etapa];
        text2.text = texto2[etapa];

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
